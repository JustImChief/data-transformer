"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDate = getDate;
exports.getFullDate = getFullDate;
exports.getFullTime = getFullTime;
exports.getTime = getTime;
exports.toArray = toArray;
exports.toBoolean = toBoolean;
exports.toDate = toDate;
exports.toInteger = toInteger;
exports.toJSON = toJSON;
exports.toNumber = toNumber;
exports.toObject = toObject;
exports.toString = toString;
exports.toText = toText;
exports.default = void 0;

var _dataValidator = _interopRequireDefault(require("@jclib/data-validator"));

var _DataTransformerError = _interopRequireDefault(require("./DataTransformerError"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @param {any} value
 * @returns {string|null}
 */
function getDate(value) {
  var date = new Date(Date.parse(value));

  if (_dataValidator.default.isDate(date)) {
    var day = date.getDate().toString();
    var month = (date.getMonth() + 1).toString();
    var year = date.getFullYear().toString();
    return "".concat(day.length > 1 ? day : "0".concat(day), ".").concat(month.length > 1 ? month : "0".concat(month), ".").concat(year);
  }

  return null;
}
/**
 * @param {*} value
 * @return {string|null}
 */


function getFullDate(value) {
  var date = getDate(value);
  var time = getFullTime(value);

  if (_dataValidator.default.isString(date) && _dataValidator.default.isString(time)) {
    return "".concat(date, " ").concat(time);
  }

  return null;
}
/**
 * @param {*} value
 * @return {string|null}
 */


function getFullTime(value) {
  var date = new Date(Date.parse(value));

  if (_dataValidator.default.isDate(date)) {
    var time = getTime(value);
    var seconds = date.getSeconds().toString();
    return "".concat(time, ":").concat(seconds.length > 1 ? seconds : "0".concat(seconds));
  }

  return null;
}
/**
 * @param {*} value
 * @return {string|null}
 */


function getTime(value) {
  var date = new Date(Date.parse(value));

  if (!!date) {
    var hours = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    return "".concat(hours.length > 1 ? hours : "0".concat(hours), ":").concat(minutes.length > 1 ? minutes : "0".concat(minutes));
  }

  return null;
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {array|*}
 * @throws {DataTransformerError}
 */


function toArray(value, defaultValue) {
  if (_dataValidator.default.isArray(value)) {
    return [...value];
  }

  if (_dataValidator.default.isObject(value)) {
    return Object.values(value);
  }

  if (_dataValidator.default.isJSON(value)) {
    try {
      var json = JSON.parse(value);
      return toArray(json, defaultValue);
    } catch (error) {
      if (!_dataValidator.default.isUndefined(defaultValue)) {
        return defaultValue;
      }

      throw new _DataTransformerError.default(error);
    }
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {boolean|*}
 * @throws {DataTransformerError}
 */


function toBoolean(value, defaultValue) {
  if (_dataValidator.default.isBoolean(value)) {
    return true === value;
  }

  if (_dataValidator.default.isNumber(value) && (value === 1 || value === 0)) {
    return 1 === value;
  }

  if (_dataValidator.default.isString(value) && (value === '1' || value === '0' || value === 'true' || value === 'false')) {
    return true === (value === '1' || value === 'true');
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {Date|*}
 * @throws {DataTransformerError}
 */


function toDate(value, defaultValue) {
  var date = new Date(value);

  if (_dataValidator.default.isDate(date)) {
    return date;
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {number|*}
 * @throws {DataTransformerError}
 */


function toInteger(value, defaultValue) {
  try {
    var number = toNumber(value, defaultValue);

    if (_dataValidator.default.isNumber(number)) {
      return Number.parseInt(number.toString(), 10);
    }
  } catch (error) {
    if (!_dataValidator.default.isUndefined(defaultValue)) {
      return defaultValue;
    }

    throw new _DataTransformerError.default(error);
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */


function toJSON(value, defaultValue) {
  try {
    var json = JSON.stringify(value);

    if (_dataValidator.default.isNotEmptyString(json)) {
      return json;
    }
  } catch (error) {
    if (!_dataValidator.default.isUndefined(defaultValue)) {
      return defaultValue;
    }

    throw new _DataTransformerError.default(error);
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {number|*}
 * @throws {DataTransformerError}
 */


function toNumber(value, defaultValue) {
  var str = toString(value, null);
  var number = Number(_dataValidator.default.isNull(str) ? void 0 : str.replace(/,/g, '.'));

  if (_dataValidator.default.isNumber(number) && !_dataValidator.default.isNaN(number)) {
    return number;
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {object|*}
 * @throws {DataTransformerError}
 */


function toObject(value, defaultValue) {
  if (_dataValidator.default.isObject(value)) {
    return _objectSpread({}, value);
  }

  if (_dataValidator.default.isArray(value)) {
    return [...value].reduce((accumulator, currentValue, currentIndex) => {
      accumulator[currentIndex] = currentValue;
      return accumulator;
    }, {});
  }

  if (_dataValidator.default.isJSON(value)) {
    try {
      var json = JSON.parse(value);
      return toObject(json, defaultValue);
    } catch (error) {
      if (!_dataValidator.default.isUndefined(defaultValue)) {
        return defaultValue;
      }

      throw new _DataTransformerError.default();
    }
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */


function toString(value, defaultValue) {
  var string = toText(value, '').replace(/(<([^>]+)>)/ig, '').trim();

  if (_dataValidator.default.isNotEmptyString(string)) {
    return string;
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}
/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */


function toText(value, defaultValue) {
  if (!_dataValidator.default.isUndefined(value) && !Number.isNaN(value) && !_dataValidator.default.isNull(value)) {
    var text = value.toString().replace(/(<p>&nbsp;<\/p>)/ig, '').replace(/(&nbsp;)/ig, ' ').trim();

    if (_dataValidator.default.isNotEmptyString(text)) {
      return text;
    }
  }

  if (!_dataValidator.default.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new _DataTransformerError.default();
}

var DataTransformer = {
  getDate,
  getFullDate,
  getFullTime,
  getTime,
  toArray,
  toBoolean,
  toDate,
  toInteger,
  toJSON,
  toNumber,
  toObject,
  toString,
  toText
};
var _default = DataTransformer;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class DataTransformerError extends Error {
  constructor(error) {
    super(error.message || error);

    _defineProperty(this, "name", 'DataTransformerError');
  }

}

var _default = DataTransformerError;
exports.default = _default;
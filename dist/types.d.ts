declare function getDate(value: any): string | null;

declare function getFullData(value: any): string | null;

declare function getFullTime(value: any): string | null;

declare function getTime(value: any): string | null;

declare function toArray<T = any>(value: any, defaultValue?: T): any[] | T;

declare function toBoolean<T = any>(value: any, defaultValue?: T): boolean | T;

declare function toDate<T = any>(value: any, defaultValue?: T): Date | T;

declare function toInteger<T = any>(value: any, defaultValue?: T): number | T;

declare function toJSON<T = any>(value: any, defaultValue?: T): string | T;

declare function toNumber<T = any>(value: any, defaultValue?: T): number | T;

declare function toObject<T = any>(value: any, defaultValue?: T): object | T;

declare function toString<T = any>(value: any, defaultValue?: T): string | T;

declare function toText<T = any>(value: any, defaultValue?: T): string | T;

declare type DataTransformer = {
  getDate(value: any): string | null;
  getFullData(value: any): string | null;
  getFullTime(value: any): string | null;
  getTime(value: any): string | null;
  toArray<T = any>(value: any, defaultValue?: T): any[] | T;
  toBoolean<T = any>(value: any, defaultValue?: T): boolean | T;
  toDate<T = any>(value: any, defaultValue?: T): Date | T;
  toInteger<T = any>(value: any, defaultValue?: T): number | T;
  toJSON<T = any>(value: any, defaultValue?: T): string | T;
  toNumber<T = any>(value: any, defaultValue?: T): number | T;
  toObject<T = any>(value: any, defaultValue?: T): object | T;
  toString<T = any>(value: any, defaultValue?: T): string | T;
  toText<T = any>(value: any, defaultValue?: T): string | T;
};

declare const DataTransformer: DataTransformer;

export default DataTransformer;
export {
  getDate,
  getFullData,
  getFullTime,
  getTime,
  toArray,
  toBoolean,
  toDate,
  toInteger,
  toJSON,
  toNumber,
  toObject,
  toString,
  toText,
};
class DataTransformerError extends Error {
  name = 'DataTransformerError';

  constructor(error) {
    super(error.message || error);
  }
}

export default DataTransformerError;
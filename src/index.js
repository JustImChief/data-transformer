import DataValidator from '@jclib/data-validator';

import DataTransformerError from './DataTransformerError';

/**
 * @param {any} value
 * @returns {string|null}
 */
function getDate(value) {
  const date = new Date(Date.parse(value));

  if (DataValidator.isDate(date)) {
    const day   = date.getDate().toString();
    const month = (date.getMonth() + 1).toString();
    const year  = date.getFullYear().toString();

    return `${day.length > 1 ? day : `0${day}`}.${month.length > 1 ? month : `0${month}`}.${year}`;
  }

  return null;
}

/**
 * @param {*} value
 * @return {string|null}
 */
function getFullDate(value) {
  const date = getDate(value);
  const time = getFullTime(value);

  if (DataValidator.isString(date) && DataValidator.isString(time)) {
    return `${date} ${time}`;
  }

  return null;
}

/**
 * @param {*} value
 * @return {string|null}
 */
function getFullTime(value) {
  const date = new Date(Date.parse(value));

  if (DataValidator.isDate(date)) {
    const time = getTime(value);

    const seconds = date.getSeconds().toString();

    return `${time}:${seconds.length > 1 ? seconds : `0${seconds}`}`;
  }

  return null;
}

/**
 * @param {*} value
 * @return {string|null}
 */
function getTime(value) {
  const date = new Date(Date.parse(value));

  if (!!date) {
    const hours   = date.getHours().toString();
    const minutes = date.getMinutes().toString();

    return `${hours.length > 1 ? hours : `0${hours}`}:${minutes.length > 1 ? minutes : `0${minutes}`}`;
  }

  return null;
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {array|*}
 * @throws {DataTransformerError}
 */
function toArray(value, defaultValue) {
  if (DataValidator.isArray(value)) {
    return [...value];
  }

  if (DataValidator.isObject(value)) {
    return Object.values(value);
  }

  if (DataValidator.isJSON(value)) {
    try {
      const json = JSON.parse(value);

      return toArray(json, defaultValue);
    } catch (error) {
      if (!DataValidator.isUndefined(defaultValue)) {
        return defaultValue;
      }

      throw new DataTransformerError(error);
    }
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {boolean|*}
 * @throws {DataTransformerError}
 */
function toBoolean(value, defaultValue) {
  if (DataValidator.isBoolean(value)) {
    return (
      true === value
    );
  }

  if (DataValidator.isNumber(value) && (
    value === 1 || value === 0
  )) {
    return (
      1 === value
    );
  }

  if (DataValidator.isString(value) && (
    value === '1' || value === '0' ||
    value === 'true' || value === 'false'
  )) {
    return (
      true === (
        value === '1' || value === 'true'
      )
    );
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {Date|*}
 * @throws {DataTransformerError}
 */
function toDate(value, defaultValue) {
  const date = new Date(value);

  if (DataValidator.isDate(date)) {
    return date;
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {number|*}
 * @throws {DataTransformerError}
 */
function toInteger(value, defaultValue) {
  try {
    const number = toNumber(value, defaultValue);

    if (DataValidator.isNumber(number)) {
      return Number.parseInt(number.toString(), 10);
    }
  } catch (error) {
    if (!DataValidator.isUndefined(defaultValue)) {
      return defaultValue;
    }

    throw new DataTransformerError(error);
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */
function toJSON(value, defaultValue) {
  try {
    const json = JSON.stringify(value);

    if (DataValidator.isNotEmptyString(json)) {
      return json;
    }
  } catch (error) {
    if (!DataValidator.isUndefined(defaultValue)) {
      return defaultValue;
    }

    throw new DataTransformerError(error);
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {number|*}
 * @throws {DataTransformerError}
 */
function toNumber(value, defaultValue) {
  const str    = toString(value, null);
  const number = Number(DataValidator.isNull(str) ? void 0 : str.replace(/,/g, '.'));

  if (DataValidator.isNumber(number) && !DataValidator.isNaN(number)) {
    return number;
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {object|*}
 * @throws {DataTransformerError}
 */
function toObject(value, defaultValue) {
  if (DataValidator.isObject(value)) {
    return {...value};
  }

  if (DataValidator.isArray(value)) {
    return [...value].reduce((accumulator, currentValue, currentIndex) => {
      accumulator[currentIndex] = currentValue;

      return accumulator;
    }, {});
  }

  if (DataValidator.isJSON(value)) {
    try {
      const json = JSON.parse(value);

      return toObject(json, defaultValue);
    } catch (error) {
      if (!DataValidator.isUndefined(defaultValue)) {
        return defaultValue;
      }

      throw new DataTransformerError();
    }
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */
function toString(value, defaultValue) {
  const string = toText(value, '')
    .replace(/(<([^>]+)>)/ig, '')
    .trim();

  if (DataValidator.isNotEmptyString(string)) {
    return string;
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

/**
 * @param {*} value
 * @param {*} [defaultValue]
 * @return {string|*}
 * @throws {DataTransformerError}
 */
function toText(value, defaultValue) {
  if (!DataValidator.isUndefined(value) && !Number.isNaN(value) && !DataValidator.isNull(value)) {
    const text = value.toString()
      .replace(/(<p>&nbsp;<\/p>)/ig, '')
      .replace(/(&nbsp;)/ig, ' ')
      .trim();

    if (DataValidator.isNotEmptyString(text)) {
      return text;
    }
  }

  if (!DataValidator.isUndefined(defaultValue)) {
    return defaultValue;
  }

  throw new DataTransformerError();
}

const DataTransformer = {
  getDate,
  getFullDate,
  getFullTime,
  getTime,
  toArray,
  toBoolean,
  toDate,
  toInteger,
  toJSON,
  toNumber,
  toObject,
  toString,
  toText,
};

export default DataTransformer;
export {
  getDate,
  getFullDate,
  getFullTime,
  getTime,
  toArray,
  toBoolean,
  toDate,
  toInteger,
  toJSON,
  toNumber,
  toObject,
  toString,
  toText,
};